package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Contato;
import negocio.ContatosService;

@Stateless
@Path("/consulta")
public class ContatosRest {
	
	@Inject
	private ContatosService contatosService;
	
	public ContatosRest() {
		// Construtor default
	}
	
	@GET
	@Path("/contatos/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Contato getContato(@PathParam("id") String id) {
		return contatosService.getContatoID(Integer.parseInt(id));
	}
	
	@GET
	@Path("/contatos")
	@Produces(MediaType.TEXT_PLAIN)
	public List<Contato> listar() {
		return contatosService.listarContatos();
	}
	
	@DELETE
	@Path("/delete")
	public Response deleteContato(@QueryParam("id") int id) {
		Contato c = contatosService.getContatoID(id);
		if (c == null) {
			throw new NotFoundException();
		}
		
		contatosService.removerContato(id);
		return Response.noContent().build();
	}
}
